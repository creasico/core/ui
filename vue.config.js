const pkg = require('./package.json')

const env = process.env.NODE_ENV || 'development'
process.env.VUE_APP_NAME = 'Creasi UI'
process.env.VUE_APP_VERSION = pkg.version
process.env.VUE_APP_LOCALE = 'en'
process.env.VUE_APP_API_URL = env === 'development'
  ? 'http://localhost:3000/api/'
  : 'https://apify.now.sh/api/'
process.env.BASE_URL = process.env.CI_PROJECT_NAME
  ? `/${process.env.CI_PROJECT_NAME}/`
  : '/'

module.exports = {
  publicPath: process.env.BASE_URL || '/',
  css: {
    loaderOptions: {
      sass: {
        data: '@import "@/styles/variables.scss";'
      }
    }
  },
  chainWebpack (config) {
    // remove the prefetch plugin
    config.plugins.delete('prefetch')

    // Add basic html head
    config.plugin('html').tap(args => {
      args[0].title = process.env.VUE_APP_NAME
      args[0].meta = {
        'robots': 'index,follow'
      }

      return args
    })

    // Update transformAssetUrl rule from vue-loader
    // according to usage of bootstrap-vue
    config.module.rule('vue').use('vue-loader').loader('vue-loader').tap(opts => {
      opts.transformAssetUrls = {
        img: 'src',
        image: 'xlink:href',
        source: 'src',
        video: ['src', 'poster'],
        'b-img': 'src',
        'b-img-lazy': ['src', 'blank-src'],
        'b-card': 'img-src',
        'b-card-img': 'img-src',
        'b-carousel-slide': 'img-src',
        'b-embed': 'src'
      }

      return opts
    })
  }
}
