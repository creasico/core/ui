import { shallowMount } from '@vue/test-utils'
import Icon from '@/components/icon.vue'

describe('icon component', () => {
  it('fallback to square icon', () => {
    let warning = ''
    console['warn'] = jest.fn(input => (warning += input))

    const name = 'foo-bar'
    const { vm } = shallowMount(Icon, {
      propsData: { name }
    })

    expect(warning).toMatch('Icon name "foo-bar" doesn\'t exist, fallback to "square" icon.')
    expect(vm.iconName).toMatch('square')
  })
})
