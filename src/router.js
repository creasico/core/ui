import Vue from 'vue'
import Qs from 'qs'
import Router from 'vue-router'
import Dashboard from '@/views/dashboard.vue'

import resourceRoutes from '@/utilities/resource-routes'

Vue.use(Router)

const routes = [
  {
    path: '/',
    name: 'dashboard',
    component: Dashboard,
    meta: {
      title: 'Dashboard',
      icon: 'home'
    }
  },

  {
    path: '/icons',
    name: 'icons',
    component: () => import(/* webpackChunkName: "pages" */ '@/views/icons.vue'),
    meta: {
      title: 'All Icons',
      description: 'All icons provided by Feather Icons - Simply beautiful open source icons',
      icon: 'grid'
    }
  },

  {
    path: '/notifications',
    name: 'notifications',
    component: () => import(/* webpackChunkName: "pages" */ '@/views/notifications.vue'),
    meta: {
      title: 'Notifications',
      icon: 'notification'
    }
  },

  ...resourceRoutes({
    posts: {
      path: '/posts',
      title: 'Posts Management',
      menu: 'Posts',
      icon: 'folder'
    },
    products: {
      path: '/products',
      title: 'Products Management',
      menu: 'Products',
      icon: 'cart'
    },
    albums: {
      path: '/albums',
      title: 'Photo Albums',
      menu: 'Albums',
      icon: 'camera',
      children: {
        images: {
          menu: 'Images',
          title: 'Album Images'
        }
      }
    },
    users: {
      path: '/users',
      title: 'Users Management',
      menu: 'Users',
      icon: 'users'
    }
  }),

  {
    path: '*',
    name: 'not-found',
    component: () => import(/* webpackChunkName: "error-404", webpackPrefetch: true */ '@/views/errors/not-found.vue'),
    meta: {
      title: 'Page not found',
      menu: false
    }
  }
]

export default new Router({
  mode: 'history',
  linkActiveClass: 'active',
  linkExactActiveClass: 'active',
  base: process.env.BASE_URL,
  parseQuery (query) {
    return Qs.parse(query, {
      indices: true,
      allowDots: true
    })
  },
  scrollBehavior (to, from, savedPosition) {
    if (to.hash) {
      return {
        selector: to.hash
      }
    }

    if (savedPosition) {
      return savedPosition
    }

    return { x: 0, y: 0 }
  },
  stringifyQuery (query) {
    const result = Qs.stringify(query, {
      indices: true,
      allowDots: true
    })

    return result ? `?${result}` : ''
  },
  routes
})
