export default {
  /**
   * Get Item Key.
   *
   * @return {String}
   */
  get KEY () {
    return 'creasi_meta'
  },

  /**
   * Retrieve all cached meta.
   *
   * @return {Object}
   */
  all () {
    return JSON.parse(localStorage.getItem(this.KEY) || '{}')
  },

  /**
   * Get meta resource from cache.
   *
   * @param  {String}  type  Resource type
   * @param  {any}  defaultValue  Detault returned value
   * @return {Object}
   */
  get (type, defaultValue = {}) {
    return this.all()[type] || defaultValue
  },

  /**
   * Add or update meta resource to cache.
   *
   * @param  {String}  type  Resource type
   * @param  {Object}  value  Resource meta
   * @return {Object}
   */
  set (type, value) {
    if (!type) return

    const meta = this.all()

    meta[type] = Object.assign({}, this.get(type), value || {})

    return localStorage.setItem(this.KEY, JSON.stringify(meta))
  }
}
