import axios from 'axios'
import Qs from 'qs'

import * as _reducer from './form-field-reducer'
import _storage from './meta-storage'

export const reducer = _reducer
export const storage = _storage

/**
 * Get site meta.
 *
 * @param  {String}  name  Site meta name.
 * @param  {any}  defaults  Site meta name.
 * @return {string|undefined}
 */
export const getMeta = (name, defaults = null) => {
  const meta = document.head.querySelector(`meta[name="${name}"]`)

  return meta ? meta.content : defaults
}

const clientConfig = {
  baseURL: getMeta('api-url', '/api/'),
  headers: {
    common: {
      'Accept': 'application/json',
      // 'Access-Control-Allow-Origin': '*',
      // 'Access-Control-Allow-Methods': 'POST, GET, PUT, DELETE',
      'Content-Type': 'application/x-www-form-urlencoded',
      'X-Requested-With': 'XMLHttpRequest'
    }
  },
  paramsSerializer (params) {
    return Qs.stringify(params, { indices: true, allowDots: true })
  }
}

const auth = storage.get('$auth', {})

if (auth.accessToken) {
  clientConfig.headers.common['Authorization'] = `Bearer ${auth.accessToken}`
}

export const client = axios.create(clientConfig)

client.interceptors.response.use(res => res, err => {
  if (err.response) {
    return {}
  }

  return Promise.reject(err)
})
