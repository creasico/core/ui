import FormField from '@/components/form-field'
import { reducer, storage } from '@/utilities'

export function data () {
  return {
    model: {}
  }
}

export const components = {
  FormField
}

export const computed = {
  ID () {
    return this.$route.params.id || null
  },

  isEditing () {
    return !!this.ID
  },

  isDisabled () {
    return false
  },

  resetBtn () {
    return this.isEditing ? 'Reset' : 'Cancel'
  },

  submitBtn () {
    return this.isEditing ? 'Update' : 'Submit'
  },

  hasChanges () {
    return !!Object.keys(this.model).length
  },

  $fields () {
    const fields = (this.meta || {}).fields || []

    return fields.map(this.normalizeField)
  },

  $meta () {
    return this.meta || storage.get(this.$route.name.split('.')[0]) || {}
  },

  panels () {
    let meta = this.meta || this.$meta

    if (meta.hasOwnProperty('panels')) return meta.panels

    meta.panels = reducer.panels(this.$fields.slice())

    storage.set(meta.resource, meta)

    return meta.panels
  }
}

export const methods = {
  async init () {
    this.resetForm()
  },

  normalizeField ({ key, label, desc, type, attrs, ...field }) {
    const item = {
      key,
      label,
      desc,
      type,
      attrs: Object.assign({}, field, attrs)
    }

    if (['panel', 'group'].includes(type)) {
      delete item.attrs.fields
      item.fields = field.fields.map(this.normalizeField)
    }

    return item
  },

  assignFormData () {
    const model = Object.assign({}, this.data || {})

    if (!this.isEditing) {
      this.$fields.forEach(item => {
        if (!item.attrs.visible) return

        if (['panel', 'group'].includes(item.type)) {
          item.fields.forEach(child => {
            if (child.attrs.visible) {
              model[child.key] = null
            }
          })
        } else {
          model[item.key] = null
        }
      })
      for (const [key, field] of Object.entries(this.$fields)) {
        model[key] = field
      }
    }

    this.model = model
  },

  resetForm () {
    this.assignFormData()
  },

  onReset () {
    this.resetForm()
  },

  async onSubmit () {
    if (this.isEditing) {
      await this.onUpdate(this.model, this.ID)
    } else {
      await this.onStore(this.model)
    }
  },

  async onUpdate (data, id) {
    // .
  },

  async onStore (data) {
    // .
  }
}
