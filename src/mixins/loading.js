import { mapGetters, mapMutations } from 'vuex'

export const computed = mapGetters(['isBusy'])

export const methods = mapMutations(['busy', 'done'])
