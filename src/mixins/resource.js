import { mapActions } from 'vuex'
import * as loading from './loading'

export const mixins = [loading]

export const computed = {
  authenticated () {
    return !!this.$route.meta && this.$store.getters.authenticated
  }
}

export const methods = {
  ...mapActions('resource', ['remove']),

  deleteItem (item) {
    if (confirm(`Are you sure wanna delete item: ${item.id}`)) {
      this.busy()

      return this.remove(item.id)
    }

    return Promise.resolve(false)
  }
}
