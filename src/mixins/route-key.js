export const computed = {
  routeKey () {
    return this.$route.path.slice(1).split('/').join('-')
  }
}
