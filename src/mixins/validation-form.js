import * as forms from './forms'

export const mixins = [forms]

export const methods = {
  /**
   * Determine is the field valid.
   *
   * @param {string} field Field key
   * @return {boolean}
   */
  isValid (field) {
    return !(this.alerts.errors && this.alerts.errors[field])
  },

  /**
   * Get field state.
   *
   * @param {string} field Field key
   * @return {boolean|null}
   */
  getFieldState (field) {
    return this.isValid(field) ? null : false
  },

  /**
   * Get field Feedbacks.
   *
   * @param {string} field Field key
   * @return {boolean|null}
   */
  getFieldFeedbacks (field) {
    return !this.isValid(field) ? this.alerts.errors[field] : null
  }
}
