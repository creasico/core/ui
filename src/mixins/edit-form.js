import * as forms from './forms'
import * as loading from './loading'

export const mixins = [forms, loading]

export const computed = {
  isEditing () {
    return true
  }
}

export const methods = {
  resetForm () {
    this.assignFormData()

    this.done()
  },

  async onSubmit () {
    this.busy()

    await this.update({
      data: this.model
    })

    this.resetForm()
  },

  async init () {
    this.busy()

    await this.show()

    this.resetForm()
  }
}
