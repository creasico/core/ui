export const computed = {
  project () {
    return {
      name: process.env.VUE_APP_NAME,
      version: `v${process.env.VUE_APP_VERSION}`
    }
  },

  credits () {
    const year = new Date().getFullYear()
    const project = `${this.project.name} ${this.project.version}`

    return `${project} &copy; ${year} All Rights Reserved.`
  }
}
