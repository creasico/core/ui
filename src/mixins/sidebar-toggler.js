export function data () {
  return {
    collapse: false
  }
}

export const methods = {
  toggleSidebar (collapse, menu = {}) {
    if (menu.hasChildren && menu.open) {
      return
    }

    this.collapse = collapse

    const $app = document.querySelector('#app')

    if ($app) {
      $app.classList.toggle('show')
    }
  },

  onTouchStart (e) {
    this.touchStart = {
      x: e.changedTouches[0].clientX,
      y: e.changedTouches[0].clientY
    }
  },

  onTouchEnd (e) {
    const dx = e.changedTouches[0].clientX - this.touchStart.x
    const dy = e.changedTouches[0].clientY - this.touchStart.y

    if (Math.abs(dx) > Math.abs(dy) && Math.abs(dx) > 40) {
      this.toggleSidebar(dx > 0 && this.touchStart.x <= 80)
    }
  }
}
