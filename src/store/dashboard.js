import { client } from '@/utilities'

export const state = {
  stats: [],
  meta: {}
}

export const mutations = {
  statFetched (state, { stats, meta }) {
    state.stats = stats
    state.meta = meta
  }
}

export const actions = {
  async fetchStats ({ commit }) {
    const { data: body } = await client.get('/dashboard')

    if (body) {
      commit('statFetched', {
        stats: body.data,
        meta: body.meta
      })
    }
  }
}
