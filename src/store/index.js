import Vue from 'vue'
import Vuex from 'vuex'
import { basename } from 'path'
import persistedState from 'vuex-persistedstate'

import { storage } from '@/utilities'

Vue.use(Vuex)

const modules = {}
const loadModules = require.context('.', false, /\.js$/)

loadModules.keys().forEach(module => {
  if (module === './index.js') return

  const key = basename(module, '.js')

  modules[key] = {
    namespaced: true,
    ...loadModules(module)
  }
})

export default new Vuex.Store({
  modules,

  state: {
    $auth: {},
    $busy: false,
    $settings: {}
  },

  plugins: [
    persistedState({
      key: storage.KEY,
      paths: ['$auth', '$settings']
    })
  ],

  getters: {
    isBusy (state) {
      return state.$busy
    },

    authenticated ({ $auth }) {
      return !!$auth.accessToken
    },

    accessToken ({ $auth }) {
      return $auth.accessToken || null
    }
  },

  mutations: {
    authenticate (state, auth) {
      state.$auth = auth
      state.$busy = false
    },

    busy (state) {
      state.$busy = true
    },

    done (state) {
      state.$busy = false
    }
  },

  actions: {
    busy ({ commit }) {
      commit('busy')
    },

    done ({ commit }) {
      commit('done')
    },

    async login ({ commit }, { username, password }) {
      commit('busy')

      if (username && password) {
        commit('authenticate', {
          username,
          accessToken: Buffer.from(username).toString('base64')
        })
      } else {
        commit('alert', {
          status: 401,
          data: 'Login gagal'
        })
      }
    },

    async register ({ commit }, credential) {
      commit('busy')
    },

    async forgot ({ commit }, credential) {
      commit('busy')
    },

    async logout ({ commit }) {
      commit('busy')

      commit('authenticate', {})
    }
  }
})
