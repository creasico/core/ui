// import { storage } from '@/utilities'

export const state = {
  data: null,
  meta: {},
  type: 'index'
}

export const getters = {
  data (state) {
    if (!state.data) {
      return state.type === 'index' ? [] : {}
    }

    return state.data
  },

  meta (state, getters, rootState) {
    // const [, type] = (rootState.route.name || '').split('.') || []
    // const [field, direction] = Object.entries(state.sort)[0]
    // const meta = state.meta || {}

    // if (type === 'index') {
    //   meta.sortBy = { field, direction }
    //   meta.current_page = Number(state.meta.current_page || 1)
    //   meta.perPage = Number(state.meta.perPage || 15)
    //   meta.total = Number(state.meta.total || 0)
    //   meta.paginated = meta.total === meta.perPage
    // } else {
    //   meta.isEditing = type === 'edit'
    // }

    return state.meta
  }
}

export const mutations = {
  fetching (state, { type, ...meta }) {
    state.data = type === 'index' ? [] : {}
    state.meta = meta
    state.type = type
  },

  fetched (state, { data, meta, type }) {
    state.data = data
    state.meta = meta
    state.type = type
  }
}

// export const actions = {
//   async create ({ commit, rootState }, { includes = [] } = {}) {
//     const [resource, type] = (rootState.route.name || '.').split('.')

//     commit('fetching', { resource, type })

//     const { data: body } = await client.get(`${resource}/create`, {
//       params: { includes }
//     })

//     if (body) {
//       commit('fetched', {
//         data: body.data,
//         meta: { resource, type, ...body.meta },
//         includes
//       })
//     }
//   },

//   async store ({ commit, rootState }, { data }) {
//     const [resource, type] = (rootState.route.name || '.').split('.')

//     commit('fetching', { resource, type })

//     const { data: body } = await client.post(resource, data)

//     if (body) {
//       commit('fetched', {
//         data: body.data,
//         meta: { resource, type, ...body.meta }
//       })
//     }
//   },

//   async show ({ commit, rootState }, { id, includes = [] } = {}) {
//     const [resource, type] = (rootState.route.name || '.').split('.')

//     commit('fetching', { resource, type })

//     const { data: body } = await client.get(`${resource}/${id}`, {
//       params: { includes }
//     })

//     if (body) {
//       commit('fetched', {
//         data: body.data,
//         meta: { resource, type, ...body.meta },
//         includes
//       })
//     }
//   },

//   async update ({ commit, rootState }, { id, data }) {
//     const [resource, type] = (rootState.route.name || '.').split('.')

//     commit('fetching', { resource, type })

//     const { data: body } = await client.put(`${resource}/${id}`, data)

//     if (body) {
//       commit('fetched', {
//         data: body.data,
//         meta: { resource, type, ...body.meta }
//       })
//     }
//   },

//   async remove ({ commit, rootState, dispatch }, id) {
//     const [resource, type] = (rootState.route.name || '.').split('.')

//     commit('fetching', { resource, type })

//     const { status, statusText } = await client.delete(`${resource}/${id}`, {
//       validateStatus (status) {
//         return status === 204
//       }
//     })

//     if (status === 204) {
//       const data = {
//         message: `Data ${id} has need deleted successfully.`
//       }

//       commit('notify', { status, statusText, data }, { root: true })

//       return true
//     }

//     return false
//   }
// }
