import { client, reducer, storage } from '@/utilities'

export const state = {
  data: {},
  meta: {}
}

export const mutations = {
  fetched (state, { data, meta }) {
    const resMeta = {
      resource: 'account'
    }

    if (!state.meta.panels) {
      resMeta.panels = reducer.panels(meta.fields || [])

      storage.set(resMeta.resource, resMeta)
    }

    state.data = data
    state.meta = Object.assign(meta, resMeta)
  },

  fetching (state) {
    state.data = {}
    state.meta = storage.get(state.meta.resource)
  }
}

export const actions = {
  async show ({ commit }) {
    commit('fetching')

    const { data } = await client.get('/account')

    if (data) {
      commit('fetched', data)
    }
  },

  async update ({ commit }, data) {
    commit('fetching')

    const { data: body, status, statusText } = await client.put('/account', data)

    if (body) {
      commit('notify', {
        status,
        statusText,
        data: {
          message: 'Settings updated'
        }
      }, { root: true })
    }
  }
}
