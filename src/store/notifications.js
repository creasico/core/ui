
export const state = {
  items: []
}

export const getters = {
  items (state) {
    return state.items.filter(item => item.show)
  }
}

export const mutations = {
  push (state, item) {
    state.items.push(item)
  },

  close (state, item) {
    item.show = false
  },

  clear (state) {
    state.items = []
  }
}
