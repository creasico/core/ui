import Vue from 'vue'
import { sync } from 'vuex-router-sync'
import BootstrapVue from 'bootstrap-vue'
import nProgress from 'nprogress'

import App from './app.vue'
import router from './router'
import store from './store'

import Icon from '@/components/icon'
import Page from '@/components/page'

Vue.use(BootstrapVue)

Vue.component(Icon.name, Icon)
Vue.component(Page.name, Page)

Vue.config.productionTip = false

sync(store, router)

nProgress.configure({
  showSpinner: false
})

const app = window.app = new Vue({
  render (h) {
    return h(App)
  },
  router,
  store
})

app.$mount('#app')
