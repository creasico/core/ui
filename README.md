# Creasi UI
[![pipeline status](https://gitlab.com/creasico/core/ui/badges/master/pipeline.svg)](https://gitlab.com/creasico/core/ui/commits/master)
[![coverage report](https://gitlab.com/creasico/core/ui/badges/master/coverage.svg)](https://gitlab.com/creasico/core/ui/commits/master)

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
